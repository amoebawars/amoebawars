using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class sceneManager : MonoBehaviourPunCallbacks
{
	//public GameObject player1;
	public GameObject[] Players;
	public GameObject[] spawnPoints;
	public bool[] playerTaken;
	// Start is called before the first frame update
	void Start()
	{
		playerTaken = new bool[4];
		for (int i = 0; i < playerTaken.Length; i++) {
			playerTaken[i] = false;
		}
	}

	// Update is called once per frame
	void Update()
	{

	}

	[PunRPC]
	public void OnStartMEssage(PhotonMessageInfo info)
	{
		Debug.Log("im the giant rat that makes all of the rules");
	}

	public void InstantiatePlayer(int playerPosition)
	{
		//GameObject p = PhotonNetwork.Instantiate("Player", new Vector3(0,0,0));

		spawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawn");

		//PhotonNetwork.Instantiate(Players[PhotonNetwork.CurrentRoom.Players.Count - 1].name, new Vector3(0, 0, 0), Quaternion.identity, 0);

		PhotonNetwork.Instantiate(Players[playerPosition].name, spawnPoints[playerPosition].transform.position, Quaternion.identity, 0);
	}
}
