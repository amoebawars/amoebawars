using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class EnemyAI : MonoBehaviourPunCallbacks
{
	public float zoomSpeed = 5f;  //movement of the enemy itself
	private bool canShoot = false; //bool for the time-out
	public int enemyHitPoints = 0;
	private int enemyHitPointsTotal = 0;
	private bool isBuffed = false;

	public GameObject[] players; //gets array of objects tag player for multiplayer
	public float distanceToPlayer = 0; //distance to the player object
	public float[] distancesToPlayers;
	public float lockOnDistance = 100f; //distance before the enemy will lock-on and try to attack the player
	private float defaultDistance = 20f; //for findest the closest player to the enemy
	public int playerIndexNumber;
	private float shortestDistance;
	public GameObject bulletPrefab; //get's the bullet
	public GameObject shootPoint; //get's the enemy shoot point

	private Bullet Bulletscript;

	private Vector2 directionToPlayer;
	private PromiseManager promise;
	//private GameObject flag;

	private GameObject buffEnemySpawnPoint;
	//private bool previouslyBuffed = false;
	AudioSource deathNoise;



	// Start is called before the first frame update
	void Start()
	{
		playerIndexNumber = 0;
		distancesToPlayers = new float[5];
		shortestDistance = defaultDistance;
		promise = GameObject.FindGameObjectWithTag("PromiseFactory").GetComponent<PromiseManager>();
		deathNoise = this.GetComponent<AudioSource>(); //death sound effect
		players = GameObject.FindGameObjectsWithTag("Player");
		if (this.gameObject.tag == "ShootEnemy")
		{
			enemyHitPointsTotal = 2;
			canShoot = true;
		}
		if (this.gameObject.tag == "Enemy")
		{
			enemyHitPoints = 2;
			enemyHitPointsTotal = 2;
			canShoot = false;
		}
		if (this.gameObject.tag == "BigEnemy")
		{
			enemyHitPoints = 3;
			enemyHitPointsTotal = 3;
			canShoot = false;
		}
		enemyHitPoints = enemyHitPointsTotal;
	}

	// Update is called once per frame
	void Update()
	{
		shortestDistance = 50;
		players = GameObject.FindGameObjectsWithTag("Player");
		int numberOfPlayers = players.Length;
		for(int i = 0; i < numberOfPlayers; i++)
        {
			distancesToPlayers[i] = (players[i].transform.position - transform.position).magnitude;
			if(distancesToPlayers[i] < shortestDistance)
            {
				shortestDistance = distancesToPlayers[i];
            }
		}
		for (int i = 0; i < numberOfPlayers; i++)
        {
			if(distancesToPlayers[i] == shortestDistance)
            {
				playerIndexNumber = i;
            }
        }
		try
		{
			if (shortestDistance < lockOnDistance)
			{
				Track(players[playerIndexNumber]);	
				if (canShoot)
				{
					promise.CreatePromise(() => { shootBullet(); }, 10f);
					canShoot = false;
				}
			}
			else
			{
				Idle();
			}
		}
		catch (MissingReferenceException e)
		{
			Debug.Log("this error has been caught dont worry");
		}
	}

	void Idle()
	{
		GameObject[] flags;
		flags = GameObject.FindGameObjectsWithTag("Flag");
		int sizeOfArray = flags.Length;
		for (int i = 0; i < sizeOfArray; i++)
		{
			Vector2 directionToFlag = flags[i].transform.position - transform.position;
			float distanceToFlag = directionToFlag.magnitude;
			if (distanceToFlag < lockOnDistance)
			{
				directionToFlag.Normalize();
				Vector3 temp = transform.position;
				temp += new Vector3(directionToFlag.x, directionToFlag.y, 0) * Time.deltaTime * zoomSpeed;
				transform.position = temp;
			}
		}
	}

	void Track(GameObject target)
	{
		Vector2 newDir = target.transform.position - transform.position;
		newDir.Normalize();
		Vector3 temp = transform.position;
		temp += new Vector3(newDir.x, newDir.y, 0) * Time.deltaTime * zoomSpeed;

		RaycastHit2D[] results = new RaycastHit2D[1];

		int hitsTotal = this.GetComponent<Collider2D>().Raycast(newDir, results, 2);
		Debug.DrawRay(this.transform.position, newDir * 2, Color.white, 10);

		if (hitsTotal != 0) //if you hit a thing
		{
			if (results[0].collider.gameObject.tag != "Wall") //if that thing is not a wall
			{

				Vector2 futurePosition = lagCompensation(this.transform.position, temp);
				transform.position = Vector3.Lerp(futurePosition, this.transform.position, 0.3f);
				//transform.position = temp; //move

			}
			else
			{
				if (results[0].collider.gameObject.tag == "Wall") //if it was a wall move the opisate direction
				{
					//transform.position -= (Vector3)newDir * 0.2f;
				}
			}
		}
		else { //if you hit nothing move anyway
			transform.position = temp;
		}
	}

	void shootBullet()
	{
		if (PhotonNetwork.IsMasterClient)
		{
			directionToPlayer = players[playerIndexNumber].transform.position - this.transform.position;
			float rotationForBullet = Mathf.Atan2(directionToPlayer.y, directionToPlayer.x);
			GameObject bullet = PhotonNetwork.Instantiate(bulletPrefab.name, shootPoint.transform.position, Quaternion.identity, 0);
			Bulletscript = bullet.GetComponent<Bullet>();
			bullet.transform.position = shootPoint.transform.position;
			Bulletscript.direction = directionToPlayer;
			//float rotationForBullet = Mathf.Atan2(directionToPlayer.y, directionToPlayer.x);
			bullet.transform.eulerAngles = new Vector3(0, 0, rotationForBullet);
			Bullet bulletScript = bullet.GetComponent<Bullet>();
			//PhotonNetwork.Destroy(bullet);
			canShoot = true;
		}
	}

	public void doDamage(string shooterName)
	{
		if (PhotonNetwork.IsMasterClient)
		{
			enemyHitPoints--;
			if (enemyHitPoints <= 0)
			{
				moveBuffedEnemy(shooterName);
			}
		}
	}
	public void buffEnemy()
	{
		if (PhotonNetwork.IsMasterClient)
		{
			if (!isBuffed)
			{
				enemyHitPointsTotal++;
				enemyHitPoints++;
				isBuffed = true;
			}
		}

	}
	private void moveBuffedEnemy(string shooterName)
	{
		Debug.Log($"{this.gameObject.name} has been killed by {shooterName} death location {this.transform.position}");
		enemyHitPoints = enemyHitPointsTotal;

		if (PhotonNetwork.OfflineMode) {
			if (isBuffed)
			{
				Constants.C.score += 200;
			}
			else {
				Constants.C.score += 100;
			}

			GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("enemySpawnPoint");

			this.transform.position = spawnPoints[(int)Mathf.Floor(Random.Range(0, spawnPoints.Length - 1))].transform.position;
			return;
		}

		if (shooterName == "Player1(Clone)")
		{
			Debug.Log("player one killed ememy");
			buffEnemySpawnPoint = GameObject.FindGameObjectWithTag("Player1buffed");
			Debug.Log($"will be going to:{buffEnemySpawnPoint.transform.position} currently at: {this.transform.position}");
			this.transform.position = buffEnemySpawnPoint.transform.position;
			Debug.Log($"position after assignment: {this.transform.position}");
		}
		else if (shooterName == "Player2(Clone)")
		{
			buffEnemySpawnPoint = GameObject.FindGameObjectWithTag("Player2buffed");
			this.transform.position = buffEnemySpawnPoint.transform.position;
		}
		else if (shooterName == "Player3(Clone)")
		{
			buffEnemySpawnPoint = GameObject.FindGameObjectWithTag("Player3buffed");
			this.transform.position = buffEnemySpawnPoint.transform.position;
		}
		else if (shooterName == "Player4(Clone)")
		{
			buffEnemySpawnPoint = GameObject.FindGameObjectWithTag("Player4buffed");
			this.transform.position = buffEnemySpawnPoint.transform.position;
		}

		this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 10f);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		GameObject go = collision.gameObject;
		if (go.tag == "Player")
		{
			Debug.Log("bullet collider with player");
			collision.gameObject.GetComponent<PlayerHealth>().doPlayerDamage();
		}
		return;
	}

	private Vector2 lagCompensation(Vector2 currentPosition, Vector2 directionMoving)
	{
		Vector2 compensatedPosition;
		double currentLag = players[0].GetComponent<PlayerMovement>().lag;
		compensatedPosition = currentPosition + directionMoving * (float)currentLag;
		return compensatedPosition;
	}
}