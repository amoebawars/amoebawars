using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class MultiplayerEndSceneManager : MonoBehaviourPunCallbacks
{
	private Text textBox;

	private PromiseManager promise;

	// Start is called before the first frame update
	void Start()
	{
		textBox = this.GetComponent<Text>();

		textBox.text = $"The winner is player {Constants.C.GameWinner - 1}\nCongrats on the OK experience";

		promise = GameObject.FindGameObjectWithTag("PromiseFactory").GetComponent<PromiseManager>();

		promise.CreatePromise(() => { this.moveToMainMenu(); }, 15f);
	}

	void moveToMainMenu()
	{
		PhotonNetwork.Disconnect();
		UnityEngine.SceneManagement.SceneManager.LoadScene("SplashSM");
	}
}
