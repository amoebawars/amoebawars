using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelStruct : MonoBehaviour
{
	public struct LevelMap {
		public GameObject FlagRoot;
		public GeneralNode[] CardinalNodes; 
	}

	public struct GeneralNode {
		public GameObject NodeRoot;
		public GameObject[] Leafes;
	}

	public GameObject[] availableLeafesPlayers;
	public GameObject[] availableLeafesEnemys;
	public GameObject[] availableGeneralNodes;
	public GameObject rootNode;

	public LevelMap Level;

	public int players;


	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	private LevelMap generateLevel() {
		LevelMap myLevel = new LevelMap();

		myLevel.FlagRoot = rootNode;

		for (int i = 0; i < 4; i++) {
			//generate level based on player
			if (i < players) {
				myLevel.CardinalNodes[i].NodeRoot = availableGeneralNodes[Random.Range(0, availableGeneralNodes.Length)];
				myLevel.CardinalNodes[i].Leafes[0] = availableLeafesPlayers[Random.Range(0, availableLeafesPlayers.Length)];
				myLevel.CardinalNodes[i].Leafes[1] = availableLeafesPlayers[Random.Range(0, availableLeafesPlayers.Length)];
				myLevel.CardinalNodes[i].Leafes[2] = availableLeafesPlayers[Random.Range(0, availableLeafesPlayers.Length)];
				continue;
			}

			//generate level based on enemy in player position
			if (i >= players) {
				myLevel.CardinalNodes[i].NodeRoot = availableGeneralNodes[Random.Range(0, availableGeneralNodes.Length)];
				myLevel.CardinalNodes[i].Leafes[0] = availableLeafesEnemys[Random.Range(0, availableLeafesEnemys.Length)];
				myLevel.CardinalNodes[i].Leafes[1] = availableLeafesEnemys[Random.Range(0, availableLeafesEnemys.Length)];
				myLevel.CardinalNodes[i].Leafes[2] = availableLeafesEnemys[Random.Range(0, availableLeafesEnemys.Length)];
				continue;
			}
		}
		return myLevel;
	}
}
