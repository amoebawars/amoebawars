using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ScoreManager : MonoBehaviour
{
	public float GameTime = 500f;
	public FlagPointCollection[] ScoreZones;
	public GameObject[] ScoreZoneGameObjects;

	private bool master;
	private bool matchInProgress = true;

	// Start is called before the first frame update
	void Start()
	{
		master = PhotonNetwork.IsMasterClient;

		//find each game object for score zone and save their script to an array
		
		
		ScoreZoneGameObjects = GameObject.FindGameObjectsWithTag("ScoreZone");
		int ScoreZoneObjects = ScoreZoneGameObjects.Length;
		ScoreZones = new FlagPointCollection[ScoreZoneObjects];
		for (int i = 0; i < ScoreZoneObjects; i++) {
			ScoreZones[i] = ScoreZoneGameObjects[i].GetComponent<FlagPointCollection>();
		}
	}

	// Update is called once per frame
	void Update()
	{
		if (matchInProgress)
		{
			if (master)
			{
				checkForWin();
			}
			GameTime -= Time.deltaTime;
		}
	}

	private void checkForWin()
	{
		if (GameTime < 0)
		{
			int winner = 0;

			for (int Checking = 0; Checking < ScoreZones.Length; Checking++)
			{
				for (int Challenger = 0; Challenger < ScoreZones.Length; Challenger++) {
					Debug.Log($"Player {Checking} points {ScoreZones[Checking].points} against player {Challenger} points {ScoreZones[Challenger].points}");
					if (ScoreZones[Checking].points < ScoreZones[Challenger].points) {
						winner = ScoreZones[Challenger].player;
					}
				}
			}
			Constants.C.GameWinner = winner;
			Constants.C.updateConstants();

			matchInProgress = false;

			Debug.Log($"player: {Constants.C.GameWinner - 1} has won");
			//load everyone to the round end scene
			PhotonNetwork.LoadLevel("MultiPlayerEndScene");
		}
	}
}
