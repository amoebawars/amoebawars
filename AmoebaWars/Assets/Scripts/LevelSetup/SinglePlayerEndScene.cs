using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SinglePlayerEndScene : MonoBehaviour
{
	public Text scoreText;
	public Text highScoreText;
	private PromiseManager promise;

	// Start is called before the first frame update
	void Start()
	{
		scoreText.text = $"Your score is: {Constants.C.score}";
		highScoreText.text = $"";

		promise = GameObject.FindGameObjectWithTag("PromiseFactory").GetComponent<PromiseManager>();

		promise.CreatePromise(() => { this.moveToMainMenu(); }, 15f);
	}

	void moveToMainMenu()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene("SplashSM");
	}
}
