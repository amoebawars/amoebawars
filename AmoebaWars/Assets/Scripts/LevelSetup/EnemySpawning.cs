using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class EnemySpawning : MonoBehaviourPunCallbacks
{
    public GameObject smallEnemy;
    public GameObject bigEnemy;
    public GameObject shootEnemy;
    public bool spawnable = true;
    private GameObject[] enemySpawnPoints;
    private int numOfEnemySpawnPoints;
    private PromiseManager promise;
    void Start()
    {
        promise = GameObject.FindGameObjectWithTag("PromiseFactory").GetComponent<PromiseManager>();
        enemySpawnPoints = GameObject.FindGameObjectsWithTag("enemySpawnPoint");
        numOfEnemySpawnPoints = enemySpawnPoints.Length;
        spawnSomeEnemiesSinglePlayer();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void spawnSomeEnemiesSinglePlayer()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            //spawnable = true;
            for (int i = 0; i < numOfEnemySpawnPoints; i++)
            {
                Vector3 enemySpawnPositionV3 = enemySpawnPoints[i].transform.position;
                int enemyTypeNum = Random.Range(1, 4);
                //1 is small enemy
                //2 is big enemy
                //3 is shoot enemy
                if (enemyTypeNum == 1)
                {
                    GameObject enemy = PhotonNetwork.Instantiate(smallEnemy.name, enemySpawnPositionV3, Quaternion.identity);
                }
                else if (enemyTypeNum == 2)
                {
                    GameObject enemy = PhotonNetwork.Instantiate(bigEnemy.name, enemySpawnPositionV3, Quaternion.identity);
                }
                else if (enemyTypeNum == 3)
                {
                    GameObject enemy = PhotonNetwork.Instantiate(shootEnemy.name, enemySpawnPositionV3, Quaternion.identity);
                }
                else
                {
                    Debug.Log("how did you do this?");
                }
            }
        }
    }
}