using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagPointCollection : MonoBehaviour
{
	private GameObject NW;
	private GameObject SE;
	private GameObject[] Flags;
	public int player = 1;
	public int points = 0;
	//1grey		2blue		3purple		4green		5yellow
	// Start is called before the first frame update
	void Start()
	{
		NW = this.transform.Find("NorthWest").gameObject;
		SE = this.transform.Find("SouthEast").gameObject;
	}

	// Update is called once per frame
	void Update()
	{
		//this should probably be set to check once a second rather than every frame
		CheckForFlags();
	}

	void CheckForFlags()
	{
		points = 0;
		Flags = GameObject.FindGameObjectsWithTag("Flag");
		for (int i = 0; i < Flags.Length; i++)
		{
			//broken out to multi ifs for readability
			if (Flags[i].GetComponent<FlagBehavior>().flagSet == player)
			{
				// is the players flag
				if (Flags[i].transform.position.x > NW.transform.position.x && Flags[i].transform.position.y < NW.transform.position.y)
				{
					// flag is right of NW and down of NW
					if (Flags[i].transform.position.x < SE.transform.position.x && Flags[i].transform.position.y > SE.transform.position.y)
					{
						//flag is left of SE and up of SE
						points++;
					}
				}
			}
		}
		Debug.Log($"i am {this.gameObject.name} and i have {points}");
		//points has been set to the score of the player
	}
}
