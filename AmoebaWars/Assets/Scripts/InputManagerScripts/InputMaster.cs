// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/InputManagerScripts/InputMaster.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputMaster : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputMaster()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputMaster"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""2d44fb9b-d755-4b54-b307-9d16e683fedf"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""74b6af65-a8ff-485c-9dc3-c0154931795b"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LookNorth"",
                    ""type"": ""Button"",
                    ""id"": ""c296ce3b-f1fe-4259-aa30-9d7c4cb05087"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LookEast"",
                    ""type"": ""Button"",
                    ""id"": ""64a5d2f5-2640-43a1-8a38-87c421c41433"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LookSouth"",
                    ""type"": ""Button"",
                    ""id"": ""8d609a4c-4930-48d2-bde8-0fc58890fc34"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LookWest"",
                    ""type"": ""Button"",
                    ""id"": ""b15e5f47-eda7-4561-ab5e-a5d5be3210bf"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ShootBullet"",
                    ""type"": ""Button"",
                    ""id"": ""cc65a903-7d08-4466-a432-0ae758752223"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ShootBuff"",
                    ""type"": ""Button"",
                    ""id"": ""133657b0-4a81-413f-9336-24805b25052f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DropFlag"",
                    ""type"": ""Button"",
                    ""id"": ""07250337-cb0d-4cf4-a010-38a1295d3b7f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Start"",
                    ""type"": ""Button"",
                    ""id"": ""fe394bb7-1434-4bf8-9745-3e18d0f76296"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Spray1"",
                    ""type"": ""Button"",
                    ""id"": ""c1911062-0b04-4c39-9e3f-c2fa3ff7630f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Spray2"",
                    ""type"": ""Button"",
                    ""id"": ""aabe9b11-7148-4846-97a4-7e6961657d4e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Spray3"",
                    ""type"": ""Button"",
                    ""id"": ""0ac3caba-4878-49ba-b7c4-773aef1048a1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Spray4"",
                    ""type"": ""Button"",
                    ""id"": ""fa367960-db5e-489b-8825-67df8b4fe47d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""78568006-08dc-4cc1-b35f-2cd50765925e"",
                    ""path"": ""2DVector(mode=2)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""efa97103-de3b-4ae5-bfd5-944622fc2dc1"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": ""AxisDeadzone"",
                    ""groups"": ""xBox PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""0c255452-8a9d-4fdd-b8b1-ad0402105cb4"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": ""AxisDeadzone"",
                    ""groups"": ""xBox PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""06b75710-6ce0-409c-ad35-34611a59a98a"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": ""AxisDeadzone"",
                    ""groups"": ""xBox PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""b83861bb-342b-47c9-a212-69777db86c80"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": ""AxisDeadzone"",
                    ""groups"": ""xBox PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""610e6675-c47c-4b5f-8e7e-d6c040c12eae"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""xBox PC"",
                    ""action"": ""LookNorth"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b0610620-3221-41af-8518-3fa144b09491"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""xBox PC"",
                    ""action"": ""LookEast"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""131be3ce-ebaa-4595-b373-ddc74d16ee2a"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""xBox PC"",
                    ""action"": ""LookSouth"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c7e013e8-5228-4761-9f9a-dcac0b5e2474"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""xBox PC"",
                    ""action"": ""LookWest"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""aa5ac813-df2e-4484-8aaf-953107bb0831"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": ""Press(pressPoint=0.6)"",
                    ""processors"": """",
                    ""groups"": ""xBox PC"",
                    ""action"": ""ShootBullet"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""54a07247-68e3-4805-bacc-377835b2350a"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": ""Press(pressPoint=0.6)"",
                    ""processors"": """",
                    ""groups"": ""xBox PC"",
                    ""action"": ""ShootBuff"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0129800a-b140-4265-b22b-a575cc1a3dc9"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""xBox PC"",
                    ""action"": ""DropFlag"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""44baa64b-8734-4e65-b7fe-af932f567aa9"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Start"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""62a8311a-78d3-47db-bef0-d71e8a3623d2"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Spray1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""298cd6f7-7eb1-4cf1-9685-3de9fc3b6f14"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Spray2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8a360924-d8f8-4c26-a85b-f3f308b6f25f"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Spray3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""38323d79-3180-4c5b-b691-a86acc77e18f"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Spray4"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""xBox PC"",
            ""bindingGroup"": ""xBox PC"",
            ""devices"": [
                {
                    ""devicePath"": ""<XInputController>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_Move = m_Player.FindAction("Move", throwIfNotFound: true);
        m_Player_LookNorth = m_Player.FindAction("LookNorth", throwIfNotFound: true);
        m_Player_LookEast = m_Player.FindAction("LookEast", throwIfNotFound: true);
        m_Player_LookSouth = m_Player.FindAction("LookSouth", throwIfNotFound: true);
        m_Player_LookWest = m_Player.FindAction("LookWest", throwIfNotFound: true);
        m_Player_ShootBullet = m_Player.FindAction("ShootBullet", throwIfNotFound: true);
        m_Player_ShootBuff = m_Player.FindAction("ShootBuff", throwIfNotFound: true);
        m_Player_DropFlag = m_Player.FindAction("DropFlag", throwIfNotFound: true);
        m_Player_Start = m_Player.FindAction("Start", throwIfNotFound: true);
        m_Player_Spray1 = m_Player.FindAction("Spray1", throwIfNotFound: true);
        m_Player_Spray2 = m_Player.FindAction("Spray2", throwIfNotFound: true);
        m_Player_Spray3 = m_Player.FindAction("Spray3", throwIfNotFound: true);
        m_Player_Spray4 = m_Player.FindAction("Spray4", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_Move;
    private readonly InputAction m_Player_LookNorth;
    private readonly InputAction m_Player_LookEast;
    private readonly InputAction m_Player_LookSouth;
    private readonly InputAction m_Player_LookWest;
    private readonly InputAction m_Player_ShootBullet;
    private readonly InputAction m_Player_ShootBuff;
    private readonly InputAction m_Player_DropFlag;
    private readonly InputAction m_Player_Start;
    private readonly InputAction m_Player_Spray1;
    private readonly InputAction m_Player_Spray2;
    private readonly InputAction m_Player_Spray3;
    private readonly InputAction m_Player_Spray4;
    public struct PlayerActions
    {
        private @InputMaster m_Wrapper;
        public PlayerActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Player_Move;
        public InputAction @LookNorth => m_Wrapper.m_Player_LookNorth;
        public InputAction @LookEast => m_Wrapper.m_Player_LookEast;
        public InputAction @LookSouth => m_Wrapper.m_Player_LookSouth;
        public InputAction @LookWest => m_Wrapper.m_Player_LookWest;
        public InputAction @ShootBullet => m_Wrapper.m_Player_ShootBullet;
        public InputAction @ShootBuff => m_Wrapper.m_Player_ShootBuff;
        public InputAction @DropFlag => m_Wrapper.m_Player_DropFlag;
        public InputAction @Start => m_Wrapper.m_Player_Start;
        public InputAction @Spray1 => m_Wrapper.m_Player_Spray1;
        public InputAction @Spray2 => m_Wrapper.m_Player_Spray2;
        public InputAction @Spray3 => m_Wrapper.m_Player_Spray3;
        public InputAction @Spray4 => m_Wrapper.m_Player_Spray4;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @LookNorth.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookNorth;
                @LookNorth.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookNorth;
                @LookNorth.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookNorth;
                @LookEast.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookEast;
                @LookEast.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookEast;
                @LookEast.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookEast;
                @LookSouth.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookSouth;
                @LookSouth.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookSouth;
                @LookSouth.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookSouth;
                @LookWest.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookWest;
                @LookWest.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookWest;
                @LookWest.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookWest;
                @ShootBullet.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnShootBullet;
                @ShootBullet.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnShootBullet;
                @ShootBullet.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnShootBullet;
                @ShootBuff.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnShootBuff;
                @ShootBuff.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnShootBuff;
                @ShootBuff.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnShootBuff;
                @DropFlag.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDropFlag;
                @DropFlag.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDropFlag;
                @DropFlag.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDropFlag;
                @Start.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStart;
                @Start.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStart;
                @Start.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStart;
                @Spray1.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray1;
                @Spray1.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray1;
                @Spray1.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray1;
                @Spray2.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray2;
                @Spray2.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray2;
                @Spray2.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray2;
                @Spray3.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray3;
                @Spray3.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray3;
                @Spray3.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray3;
                @Spray4.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray4;
                @Spray4.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray4;
                @Spray4.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpray4;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @LookNorth.started += instance.OnLookNorth;
                @LookNorth.performed += instance.OnLookNorth;
                @LookNorth.canceled += instance.OnLookNorth;
                @LookEast.started += instance.OnLookEast;
                @LookEast.performed += instance.OnLookEast;
                @LookEast.canceled += instance.OnLookEast;
                @LookSouth.started += instance.OnLookSouth;
                @LookSouth.performed += instance.OnLookSouth;
                @LookSouth.canceled += instance.OnLookSouth;
                @LookWest.started += instance.OnLookWest;
                @LookWest.performed += instance.OnLookWest;
                @LookWest.canceled += instance.OnLookWest;
                @ShootBullet.started += instance.OnShootBullet;
                @ShootBullet.performed += instance.OnShootBullet;
                @ShootBullet.canceled += instance.OnShootBullet;
                @ShootBuff.started += instance.OnShootBuff;
                @ShootBuff.performed += instance.OnShootBuff;
                @ShootBuff.canceled += instance.OnShootBuff;
                @DropFlag.started += instance.OnDropFlag;
                @DropFlag.performed += instance.OnDropFlag;
                @DropFlag.canceled += instance.OnDropFlag;
                @Start.started += instance.OnStart;
                @Start.performed += instance.OnStart;
                @Start.canceled += instance.OnStart;
                @Spray1.started += instance.OnSpray1;
                @Spray1.performed += instance.OnSpray1;
                @Spray1.canceled += instance.OnSpray1;
                @Spray2.started += instance.OnSpray2;
                @Spray2.performed += instance.OnSpray2;
                @Spray2.canceled += instance.OnSpray2;
                @Spray3.started += instance.OnSpray3;
                @Spray3.performed += instance.OnSpray3;
                @Spray3.canceled += instance.OnSpray3;
                @Spray4.started += instance.OnSpray4;
                @Spray4.performed += instance.OnSpray4;
                @Spray4.canceled += instance.OnSpray4;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);
    private int m_xBoxPCSchemeIndex = -1;
    public InputControlScheme xBoxPCScheme
    {
        get
        {
            if (m_xBoxPCSchemeIndex == -1) m_xBoxPCSchemeIndex = asset.FindControlSchemeIndex("xBox PC");
            return asset.controlSchemes[m_xBoxPCSchemeIndex];
        }
    }
    public interface IPlayerActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnLookNorth(InputAction.CallbackContext context);
        void OnLookEast(InputAction.CallbackContext context);
        void OnLookSouth(InputAction.CallbackContext context);
        void OnLookWest(InputAction.CallbackContext context);
        void OnShootBullet(InputAction.CallbackContext context);
        void OnShootBuff(InputAction.CallbackContext context);
        void OnDropFlag(InputAction.CallbackContext context);
        void OnStart(InputAction.CallbackContext context);
        void OnSpray1(InputAction.CallbackContext context);
        void OnSpray2(InputAction.CallbackContext context);
        void OnSpray3(InputAction.CallbackContext context);
        void OnSpray4(InputAction.CallbackContext context);
    }
}
