﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour
{
	public static Constants C;
	//single player only
	public int score;
	//multiplayer only
	public int players;
	public int[] playerID;
	public int GameWinner;

	public class ConstantsInfo {
		//single player only
		public int score;
		//multiplayer only
		public int players;
		public int[] playerID;
		public int GameWinner;
	}


	private void Awake()
	{
		if (Constants.C == null)
		{
			Constants.C = this;
			Debug.Log("Constants init");
			initVars();
		}
	}


	private void initVars() {
		score = 0;
	}

	//parse the string to call RPC
	//this should only be called by master client
	public void updateConstants() {
		ConstantsInfo sendMe = new ConstantsInfo();
		sendMe.playerID = C.playerID;
		sendMe.players = C.players;
		sendMe.score = C.score;
		sendMe.GameWinner = C.GameWinner;
		string toSend = JsonUtility.ToJson(sendMe);

		Debug.Log(toSend);

		//PhotonView.Get(this).RPC("RPCupdateConstants", RpcTarget.AllBuffered, toSend);

		GameObject.FindGameObjectWithTag("constantManager").GetComponent<constantManager>().updateConstants(toSend);
	}
}
