using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class UpdateConstantsAfter10 : MonoBehaviourPunCallbacks
{
	// Start is called before the first frame update
	public float countdown = 10;
	private bool counting = true;
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (counting)
		{
			if (countdown < 0)
			{
				Constants.C.updateConstants();
				counting = false;
			}
			else
			{
				countdown -= Time.deltaTime;
			}
		}

	}
}
