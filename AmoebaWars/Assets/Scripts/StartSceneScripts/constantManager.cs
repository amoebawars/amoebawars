using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class constantManager : MonoBehaviour
{
	public class ConstantsInfo
	{
		//single player only
		public int score;
		//multiplayer only
		public int players;
		public int[] playerID;
		public int GameWinner;
	}


	public void updateConstants(string JSONmessage) {
		PhotonView.Get(this).RPC("RPCupdateConstants", RpcTarget.AllBuffered, JSONmessage);
	}


	[PunRPC]
	public void RPCupdateConstants(string JSONstring)
	{
		Debug.Log($"constants overwritten from RPC: {JSONstring}");
		ConstantsInfo myObj = JsonUtility.FromJson<ConstantsInfo>(JSONstring);
		Constants.C.playerID = myObj.playerID;
		Constants.C.players = myObj.players;
		Constants.C.score = myObj.score;
		Constants.C.GameWinner = myObj.GameWinner;
	}
}
