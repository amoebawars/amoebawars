using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPlaySMScript : MonoBehaviour
{
    public InputMaster InputManager;


    private void Awake()
    {
        InputManager = new InputMaster();
        InputManager.Player.Start.performed += ctx => { moveToMenu(); };
    }
    private void OnEnable()
    {
        InputManager.Enable();
    }
    private void OnDisable()
    {
        InputManager.Disable();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //on pressing start: move to SplashSM
    }

    public void moveToMenu()
    {
        Debug.Log("Happenign");
        UnityEngine.SceneManagement.SceneManager.LoadScene("SplashSM");
    }
}
