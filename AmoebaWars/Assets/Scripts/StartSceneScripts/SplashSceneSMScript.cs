using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashSceneSMScript : MonoBehaviour
{
    public GameObject[] buttons;
    
    public Text amoebaText;
    public Text ControlsText;

    public GameObject amoeba;

    private Vector3 newAmoebaPosition = new Vector3(6.53f, -4.38f, 0);
    private bool playText = false;
    private bool startPressed = true;

    public InputMaster InputManager;

    private void Awake()
    {
        InputManager = new InputMaster();
        InputManager.Player.Start.performed += ctx => { moveToMenu(); };
    }
    // Start is called before the first frame update
    void Start()
    {
        buttons = GameObject.FindGameObjectsWithTag("MenuButton");
        amoebaText.text = "";
        //modeSelectText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        //this is janky
        StartCoroutine(amoebaThreat());
        if (playText)
        {
            moveAmoeba();   
        }



     }

    IEnumerator amoebaThreat()
    {
        yield return new WaitForSeconds(5f);
        playText = true;
    }

    void moveAmoeba()
    {
        //code to move amoeba upwards
        amoeba.transform.position = newAmoebaPosition;
        playText = false;
        amoebaText.text = "Play the D@(N game";
    }

    public void moveToMenu()
    {

    }
}
