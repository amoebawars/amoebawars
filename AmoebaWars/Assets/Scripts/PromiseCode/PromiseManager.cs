using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromiseManager : MonoBehaviour
{
	public delegate void VoidFunctionHandler();
	public VoidFunctionHandler testFunction;

	//NOTE: you can also just wire the promiseHandler prefab into this to simplify this code a ton see comment for example of simplified code
	public Object promiseTemplate;
	// Start is called before the first frame update
	void Start()
	{
		promiseTemplate = Resources.Load("PromiseHolder", typeof(GameObject));
	}

	public void CreatePromise(VoidFunctionHandler voidFunction, float timeTo)
	{
		Promise promise = (GameObject.Instantiate(promiseTemplate) as GameObject).GetComponent<Promise>();
		promise.assignFunction(() => { voidFunction(); }, timeTo);
	}

}
