﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Promise : MonoBehaviour
{
	public delegate void VoidFunctionHandler();
	public VoidFunctionHandler testFunction;
	private float CountDown;

	/* 
	 Function Purpose: Exicute a function after X seconds in a friendly way, AKA coroutines are scary
	 paramiters: F function to exicute when time is up, CallIn how many seconds till function is called, float values less then 0 will be called next frame
	 */
	public void assignFunction(VoidFunctionHandler F, float CallIn) {
		testFunction = F;
		CountDown = CallIn;
		StartCoroutine("WaitForTimer");
	}
	public void runFunction() {
		testFunction();
		cleanUp();
	}

	public void cleanUp() {
		Destroy(this.gameObject);
	}

	public IEnumerator WaitForTimer() {
		if (CountDown < 0) {
			yield return null;
		} else {
			yield return new WaitForSeconds(CountDown);
		}
		runFunction();
	}

}
