using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromiseTester : MonoBehaviour
{
	//this is what you put in place of normal coroutine call
	private PromiseManager promise;
	void Start()
	{
		promise = GameObject.FindGameObjectWithTag("PromiseFactory").GetComponent<PromiseManager>();
		promise.CreatePromise(()=> { Debug.Log("this runs 10 seconds in "); }, 10f);
	}
}
