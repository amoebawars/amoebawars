using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class PlayerMovement : MonoBehaviourPunCallbacks, IPunObservable
{
	public float speed = 5f;
	private Rigidbody2D rb;
	public int playerID = 0; //set via prefab

	public Vector2 movingIn;

	private bool hasFlag = false;

	private Vector2 receivedPosition;
	private Vector2 simulatedPosition;
	//private Quaternion receivedRotation;

	private movingAverage MovingAverage;

	private int frameCounter;
	public double lag;


	public void move(Vector2 Stick)
	{
		float magnitude = Stick.magnitude;

		if (Stick.magnitude < 1)
		{
			movingIn = Stick;
		}
		else
		{
			movingIn = Stick.normalized;
		}
	}

	void Start()
	{
		MovingAverage = this.GetComponent<movingAverage>();
		//get rigidbody
		rb = this.GetComponent<Rigidbody2D>();
	}

	private void Update()
	{
		if (!photonView.IsMine)
		{
			frameCounter++;
			OnMessageKinematic();
		}
		else
		{
			moveMe();
		}
	}


	private void moveMe()
	{
		RaycastHit2D[] hits = new RaycastHit2D[10];
		int totalHits;
		ContactFilter2D filter = new ContactFilter2D();

		totalHits = Physics2D.Raycast(this.transform.position, movingIn, filter.NoFilter(), hits, 1f);
		Debug.DrawRay(this.transform.position, movingIn, Color.blue, 10f);

		bool canMove = true;

		for (int i = 0; i < totalHits; i++)
		{
			if (hits[i].collider.gameObject.tag == "Wall")
			{
				canMove = false;
			}
		}

		if (canMove)
		{
			transform.position += (Vector3)movingIn * Time.deltaTime * speed; //move
		}

	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (photonView.IsMine && !hasFlag)
		{

			Debug.Log("local collide");
			if (collision.gameObject.tag == "Flag")
			{
				if (collision.gameObject.GetComponent<FlagBehavior>().isPickable)
				{
					PhotonView.Get(this).RPC("SetFlagChild", RpcTarget.OthersBuffered, collision.gameObject.name, playerID);
				}
				Debug.Log("Hit Flag");

				collision.gameObject.GetComponent<FlagBehavior>().setParent(this.gameObject, playerID);
				hasFlag = true;
							
			}

		}
	}

	public void dropFlag()
	{
		//refactor something is causing a desync with how players bump into each other 
		if (hasFlag)
		{
			Debug.Log("player move drop");

			for (int i = 0; i < this.transform.childCount; i++)
			{
				if ("Flag" == this.transform.GetChild(i).tag)
				{
					this.transform.GetChild(i).GetComponent<FlagBehavior>().dropFlag();
				}
			}


			//this.transform.Find("Flag").GetComponent<FlagBehavior>().dropFlag();
			hasFlag = false;

			PhotonView.Get(this).RPC("RPCHasFlagOverride", RpcTarget.OthersBuffered, false);
		}
	}

	[PunRPC]
	public void RPCHasFlagOverride(bool state)
	{
		Debug.Log($"remote is changing has flag to {state}");
		hasFlag = false;
	}

	[PunRPC]
	public void SetFlagChild(string flagName, int color)
	{
		GameObject flag = GameObject.Find(flagName);
		Debug.Log($"flag: {flag.ToString()} has been collected by player: {color}");
		flag.transform.parent = this.gameObject.transform;
		flag.GetComponent<FlagBehavior>().setFlagColor(color);
		flag.transform.position = this.transform.Find("ShootPointNorth").transform.position;
		flag.GetComponent<FlagBehavior>().isPickable = false;
	}
	private void OnMessagePosition()
	{
		transform.position = Vector3.Lerp(receivedPosition, transform.position, 0.9f);
		//transform.rotation = Quaternion.Lerp(receivedRotation, transform.rotation, 0.2f);
	}
	private void OnMessageKinematic()
	{
		//transform.rotation = Quaternion.Lerp(transform.rotation, receivedRotation, 0.9f);
		//transform.position = Vector3.Lerp(transform.position, simulatedPosition, 0.2f);
		//set move me here
		simulatedPosition = receivedPosition + movingIn * (float)lag * frameCounter;
		//you could redo raycast logic for remote here should you want to for better lag compensation
		OnMessagePosition();
	}
	private double calculateLag(PhotonMessageInfo info)
	{
		double l = PhotonNetwork.Time - info.SentServerTime;
		l = Mathf.Abs((float)l);
		MovingAverage.ComputeMovingAverage(l);
		return l;
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
			stream.SendNext((Vector2)this.transform.position);
			stream.SendNext(movingIn);
		}
		else if (stream.IsReading)
		{
			receivedPosition = (Vector2)stream.ReceiveNext();
			movingIn = (Vector2)stream.ReceiveNext();
			frameCounter = 1;

			lag = calculateLag(info);
		}
	}
}
