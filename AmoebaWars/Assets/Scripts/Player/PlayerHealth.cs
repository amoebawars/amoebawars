using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerHealth : MonoBehaviourPunCallbacks
{
	// Start is called before the first frame update
	private PlayerDeathScript playerDeath;
	public int playerHealth = 3;
	public Sprite[] HealthStates;

	void Start()
	{
		playerDeath = this.GetComponent<PlayerDeathScript>();
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void doPlayerDamage()
	{
		if (photonView.IsMine)
		{
			Debug.Log("bullet hit local do damage");
			playerHealth--;
			photonView.RPC("setRemoteHealth", RpcTarget.OthersBuffered, playerHealth);
			if (playerHealth > 0)
			{
				this.GetComponent<SpriteRenderer>().sprite = HealthStates[playerHealth];
			}
			if (playerHealth <= 0)
			{
				if (PhotonNetwork.OfflineMode) {
					PhotonNetwork.LoadLevel("EndSceneSingle");
				}

				this.GetComponent<SpriteRenderer>().sprite = HealthStates[0];
				playerDeath.startPlayerTimeout();
			}
		}
		else {
			Debug.Log("bullet hit remote do nothing");
		}
	}

	[PunRPC]
	public void setRemoteHealth(int Health)
	{
		playerHealth = Health;
		this.GetComponent<SpriteRenderer>().sprite = HealthStates[playerHealth];
	}

	public void revive() {
		if (photonView.IsMine)
		{
			playerHealth = 3;
			this.GetComponent<SpriteRenderer>().sprite = HealthStates[playerHealth];
			photonView.RPC("setRemoteHealth", RpcTarget.OthersBuffered, playerHealth);
		}
	}
}
