using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerInputManager : MonoBehaviourPunCallbacks
{
	public InputMaster InputManager;

	private PlayerMovement MoveScript;
	private PlayerShooting ShootScript;
	private PlayerSpray SprayScript;

	// dont look at scary unga bunga
	private void Awake()
	{
		if (photonView.IsMine) {
			MoveScript = this.GetComponent<PlayerMovement>();
			ShootScript = this.GetComponent<PlayerShooting>();
			SprayScript = this.GetComponent<PlayerSpray>();
			InputManager = new InputMaster();

			InputManager.Player.Move.performed += ctx => MoveScript.move(ctx.ReadValue<Vector2>()); //make copies of this

			InputManager.Player.LookNorth.performed += ctx => ShootScript.SetDirection(0);
			InputManager.Player.LookEast.performed += ctx => ShootScript.SetDirection(1);
			InputManager.Player.LookSouth.performed += ctx => ShootScript.SetDirection(2);
			InputManager.Player.LookWest.performed += ctx => ShootScript.SetDirection(3);

			InputManager.Player.ShootBullet.started += ctx => ShootScript.ShootBullet();
			InputManager.Player.ShootBuff.started += ctx => ShootScript.ShootBuff();

			InputManager.Player.DropFlag.started += ctx => MoveScript.dropFlag();

			InputManager.Player.Spray1.started += ctx => SprayScript.Spray(0);
			InputManager.Player.Spray2.started += ctx => SprayScript.Spray(1);
			InputManager.Player.Spray3.started += ctx => SprayScript.Spray(2);
			InputManager.Player.Spray4.started += ctx => SprayScript.Spray(3);

			Debug.Log($"this is: {this.ToString()}");
		}

	}
	private void OnEnable()
	{
		if (photonView.IsMine)
		{
			InputManager.Enable();
		}
	}
	private void OnDisable()
	{
		if (photonView.IsMine)
		{
			InputManager.Disable();
		}
	}
}
