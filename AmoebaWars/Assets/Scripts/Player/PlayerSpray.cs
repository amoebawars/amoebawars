using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSpray : MonoBehaviourPunCallbacks
{
	public GameObject[] Sprays;
	private Queue<GameObject> SprayQueue;
	// Start is called before the first frame update
	void Start()
	{
		SprayQueue = new Queue<GameObject>();
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void Spray(int pos) {
		//this works when network gets added	
		//GameObject spray = PhotonNetwork.Instantiate(Sprays[pos].name, this.transform.position, Quaternion.identity);
		GameObject spray = PhotonNetwork.Instantiate(Sprays[pos].name, this.transform.position, Quaternion.identity);
		SprayQueue.Enqueue(spray);
		if (SprayQueue.Count > 5) {
			//replace with photon destroy	
			PhotonNetwork.Destroy(SprayQueue.Dequeue().gameObject);
		}
	}
}
