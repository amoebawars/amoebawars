using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeathScript : MonoBehaviour
{
	private bool isDead = false;

	public GameObject GBJSpawnPoint;
	public Vector3 spawnLocation;
	//public Vector2 GBJLocation;
	private int timeOutDuration = 10;

	AudioSource deathSound;
	// Start is called before the first frame update
	void Start()
	{
		spawnLocation = this.transform.position;
		deathSound = this.GetComponent<AudioSource>(); //shoot sound effect
		GBJSpawnPoint = GameObject.FindGameObjectWithTag("GBJSpawnPoint");
		//GBJLocation = GBJSpawnPoint.transform.position;
	}

	// Update is called once per frame
	void Update()
	{
		if (isDead)
		{
			deathSound.Play();
			this.transform.position = GBJSpawnPoint.transform.position;
			this.GetComponent<PlayerHealth>().playerHealth = int.MaxValue;
			StartCoroutine("welcomeToGBJ");
		}
	}

	public void startPlayerTimeout()
	{
		isDead = true;
	}

	IEnumerator welcomeToGBJ()
	{
		isDead = false;
		yield return new WaitForSecondsRealtime(timeOutDuration);
		this.GetComponent<PlayerHealth>().revive();
		this.transform.position = spawnLocation;
	}
}
