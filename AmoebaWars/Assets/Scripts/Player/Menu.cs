using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
	public InputMaster InputManager;

	public GameObject[] buttons;
	public int posint = 0;
	// 0 single player
	// 1 multi player
	// 2 hot to play

	private PromiseManager promise;

	private bool MenuSelector = true;

	// dont look at scary unga bunga
	private void Awake()
	{
		InputManager = new InputMaster();

		InputManager.Player.Move.performed += ctx => { UpdateGlowPos(ctx.ReadValue<Vector2>()); }; //make copies of this
		InputManager.Player.ShootBullet.performed += ctx => { selectButton(); }; 
	}
	private void OnEnable()
	{
		InputManager.Enable();
	}
	private void OnDisable()
	{
		InputManager.Disable();
	}

	private void Update()
	{

	}

	private void Start()
	{
		buttons = GameObject.FindGameObjectsWithTag("MenuButton");
		promise = GameObject.FindGameObjectWithTag("PromiseFactory").GetComponent<PromiseManager>();
	}

	private void UpdateGlowPos(Vector2 pos)
	{
		if (MenuSelector)
		{

			if (pos.y > 0)
			{
				MenuSelector = false;
				//move up
				Debug.Log("up");
				promise.CreatePromise(() => { resetMenu(); }, 1f);
				if (posint == 0) {
					posint = buttons.Length - 1;
				} else {
					posint--;
				}
				this.transform.position = buttons[posint].transform.position;
				this.transform.localScale = buttons[posint].transform.localScale;
			} 
			else if (pos.y < 0)
			{
				MenuSelector = false;
				//move down
				Debug.Log("down");
				promise.CreatePromise(() => { resetMenu(); }, 0.5f);
				if (posint == buttons.Length - 1)
				{
					posint = 0;
				} else {
					posint++;
				}
				this.transform.position = buttons[posint].transform.position;
				this.transform.localScale = buttons[posint].transform.localScale;
			}
			else
			{
				//do nothing
			}
		}
	}

	public void resetMenu()
	{
		MenuSelector = true;
	}

	public void selectButton() {
		Debug.Log($"Moving to Scene {buttons[posint].name}");
		switch (buttons[posint].name)
        {
			case "SinglePlayer":
				UnityEngine.SceneManagement.SceneManager.LoadScene("SinglePlayerLoader");
				break;
			case "MultiPlayer":
				Debug.Log("Transition to multiplayer when working");
				UnityEngine.SceneManagement.SceneManager.LoadScene("LobbyScene");
				break;
			case "HowToPlay":
				UnityEngine.SceneManagement.SceneManager.LoadScene("HowToPlayScene");
				break;
		}
	}
}
