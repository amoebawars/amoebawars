using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class VectorTrackCamera : MonoBehaviourPunCallbacks
{
	public GameObject player;
	public float playerTrackDistanceX;
	public float playerTrackDistanceY;
	public Vector2 directionToPlayer;

	private void Start()
	{
		player = this.transform.parent.gameObject; //get player
		this.transform.parent = null;
		//if this is my view enable this camera
		if (photonView.IsMine) {
			this.GetComponent<AudioListener>().enabled = true;
			this.GetComponent<Camera>().enabled = true;
		}
	}

	private void Update()
	{
		checkForCameraMove();
	}

	void checkForCameraMove() {
		directionToPlayer = player.transform.position -  this.transform.position ;

		if (Mathf.Abs(directionToPlayer.y) > playerTrackDistanceY || Mathf.Abs(directionToPlayer.x) > playerTrackDistanceX)
		{
			moveCamera(directionToPlayer);
		}
	}

	void moveCamera(Vector2 MoveIn) {

		transform.position += (Vector3)MoveIn * 1.5f * Time.deltaTime;
	}
}
