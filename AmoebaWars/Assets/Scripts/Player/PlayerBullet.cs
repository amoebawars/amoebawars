using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
	// Start is called before the first frame update
	public Sprite bullet;
	public Sprite buff;
	public float DeathTimeout;

	//identifying the player who shot the sheriff
	public GameObject shooterObject;

	//true is buff, false is bullet
	public bool type;

	private SpriteRenderer sprite;

	public Vector2 direction;
	public float speed;

	private PromiseManager promise;

	public Material normal_bullet_material;
	public Material buff_bullet_material;
	//private Material bullet_Material;
	//private ParticleSystemRenderer psr;

	AudioSource shootNoise;

	void Start()
	{

		//var ps = GetComponent<ParticleSystem>();
		//bullet_Material = GetComponent<Renderer>().material;
		//psr = GetComponent<ParticleSystemRenderer>();
		


		shootNoise = this.GetComponent<AudioSource>();
		sprite = this.GetComponent<SpriteRenderer>();
		if (type)
		{
			//psr.material = Resources.Load<Material>(buff_bullet_material.name);
			shootNoise.Play();
			sprite.sprite = buff;
		}
		else
		{
			//psr.material = Resources.Load<Material>(normal_bullet_material.name);
			shootNoise.Play();
			sprite.sprite = bullet;
		}

		promise = GameObject.FindGameObjectWithTag("PromiseFactory").GetComponent<PromiseManager>();
		promise.CreatePromise(() => { this.killSelf(); }, DeathTimeout);
	}

	// Update is called once per frame
	void Update()
	{
		this.transform.position += (Vector3)direction * speed * Time.deltaTime;
	}

	public void killSelf()
	{
		if (this != null)
		{
			Destroy(this.gameObject);
		}
	}



	private void OnTriggerEnter2D(Collider2D collision)
	{
		Debug.Log(collision.gameObject.name);
		if (collision.gameObject.tag == "Wall")
		{
			Destroy(this.gameObject);
		}

		if ((collision.gameObject.tag == "BigEnemy" || collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "ShootEnemy") && type == false)
		{
			collision.gameObject.GetComponent<EnemyAI>().doDamage(shooterObject.name);
			Destroy(this.gameObject);
		}

		if ((collision.gameObject.tag == "BigEnemy" || collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "ShootEnemy") && type == true)
		{
			collision.gameObject.GetComponent<EnemyAI>().buffEnemy();
			Destroy(this.gameObject);
		}

		if (collision.gameObject.tag == "Player" && collision.gameObject != shooterObject)
		{
			collision.gameObject.GetComponent<PlayerHealth>().doPlayerDamage();
			Destroy(this.gameObject);
		}
	}
}