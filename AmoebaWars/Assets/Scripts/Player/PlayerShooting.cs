using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerShooting : MonoBehaviourPunCallbacks, IPunObservable
{
	public GameObject[] ShootPoints;
	public bool[] direction;
	private int LookDirection = 2;
	private bool canShoot = true;
	public float shotTimeout = 1;

	private GameObject theShooter;

	private PlayerMovement PlayerMoveScript;

	private bool ShotType;

	public GameObject Bullet;

	public float offTilt = 0;
	public float onTilt = 0;

	private PromiseManager promise;

	public GameObject gun;


	// Start is called before the first frame update
	void Start()
	{
		theShooter = this.gameObject;

		ShootPoints = new GameObject[4];
		for (int i = 0; i < this.transform.childCount; i++)
		{
			if (this.transform.GetChild(i).gameObject.tag == "ShootPoint")
			{
				ShootPoints[i] = this.transform.GetChild(i).gameObject;
			}
		}

		PlayerMoveScript = this.GetComponent<PlayerMovement>();
		promise = GameObject.FindGameObjectWithTag("PromiseFactory").GetComponent<PromiseManager>();
		gun = this.transform.Find("Gun").gameObject;
	}

	// Update is called once per frame
	void Update()
	{

	}

	//direction is an int of 0 - 3 for north, east, south, west
	public void SetDirection(int direction)
	{
		LookDirection = direction;
		gun.transform.position = ShootPoints[LookDirection].transform.position;
		gun.transform.rotation = ShootPoints[LookDirection].transform.rotation;
	}

	public void ShootBullet()
	{
		Debug.Log($"score: {Constants.C.score}");
		ShotType = false;
		if (canShoot)
		{
			photonView.RPC("Shoot", RpcTarget.AllBuffered, PlayerMoveScript.movingIn, gameObject.name, ShotType, LookDirection);
			//Shoot();
		}
	}

	public void ShootBuff()
	{
		ShotType = true;
		if (canShoot)
		{
			photonView.RPC("Shoot", RpcTarget.AllBuffered, PlayerMoveScript.movingIn, gameObject.name, ShotType, LookDirection);
			//Shoot();
		}
	}

	[PunRPC]
	private void Shoot(Vector2 dir, string player, bool buff, int shootPoint)
	{
		canShoot = false;
		//GameObject shot = PhotonNetwork.Instantiate("bad_programmer", ShootPoints[LookDirection].transform.position, Quaternion.identity);//Instantiate(Bullet); //make bullet
		GameObject shot = GameObject.Instantiate(Bullet);
		shot.GetComponent<PlayerBullet>().shooterObject = GameObject.Find(player); //tells the bullet who shot the gun
		shot.GetComponent<PlayerBullet>().type = buff; //set bullet type

		shot.transform.position = ShootPoints[shootPoint].transform.position; //set bullet position

		switch (shootPoint)
		{
			case 0:
				shot.GetComponent<PlayerBullet>().direction = (dir * 2 + new Vector2(0, offTilt)).normalized;
				break;
			case 1:
				shot.GetComponent<PlayerBullet>().direction = (dir * 2 + new Vector2(offTilt, 0)).normalized;
				break;
			case 2:
				shot.GetComponent<PlayerBullet>().direction = (dir * 2 + new Vector2(0, -offTilt)).normalized;
				break;
			case 3:
				shot.GetComponent<PlayerBullet>().direction = (dir * 2 + new Vector2(-offTilt, 0)).normalized;
				break;
			default:
				break;
		}
		promise.CreatePromise(() => { restoreShoot(); }, shotTimeout);
	}

	public void restoreShoot()
	{
		canShoot = true;
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting && gun != null)
		{
			stream.SendNext(LookDirection);
		}
		else
		{
			LookDirection = (int)stream.ReceiveNext();

			gun.transform.position = ShootPoints[LookDirection].transform.position;
			gun.transform.rotation = ShootPoints[LookDirection].transform.rotation;

		}
	}
}
