using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerLoader : MonoBehaviour
{
	// Start is called before the first frame update
	void Start()
	{
		for (int i = 0; i < Constants.C.playerID.Length; i++)
		{
			if (Constants.C.playerID[i] == PhotonNetwork.LocalPlayer.ActorNumber) {
				this.GetComponent<sceneManager>().InstantiatePlayer(i);
			}
		}
	}
}
