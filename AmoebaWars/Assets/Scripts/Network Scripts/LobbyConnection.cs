using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class LobbyConnection : MonoBehaviourPunCallbacks
{
	public Player[] players;
	public int[] playerIDs;
	public byte maxPlayersPerRoom = 4;
	public GameObject playerPrefab;

	public int numOfPlayers = 0;
	// Start is called before the first frame update

	private bool isConnecting;
	private PromiseManager promise;

	private void Awake()
	{
		Connect();
		PhotonNetwork.AutomaticallySyncScene = true;
	}

	void Start()
	{
		playerIDs = new int[4];
	}

	// Update is called once per frame
	public void Disconnect()
	{
		PhotonNetwork.Disconnect();
	}

	public void Connect()
	{
		isConnecting = PhotonNetwork.ConnectUsingSettings();
		if (PhotonNetwork.IsConnected)
		{
			PhotonNetwork.JoinRandomRoom();
		}
		else
		{
			PhotonNetwork.ConnectUsingSettings();
		}
		//PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(Random.Range(0, 2), 2f, Random.Range(0, 2)), Quaternion.identity, 0);
	}

	void Update()
	{
		try
		{
			numOfPlayers = PhotonNetwork.CurrentRoom.PlayerCount; //tells you number of players currently in the room
		}
		catch { 
		
		}
		if (numOfPlayers == 4)
		{
			if (PhotonNetwork.IsMasterClient)
			{
				players = PhotonNetwork.PlayerList;
				for (int i = 0; i < players.Length; i++)
				{
					playerIDs[i] = players[i].ActorNumber;
				}
				Constants.C.playerID = playerIDs;

				sendToLevel(4);
				//PhotonNetwork.LoadLevel("4Players");
			}
		}
	}

	public override void OnConnectedToMaster()
	{
		base.OnConnectedToMaster();
		Debug.Log("Connected to master");
		if (isConnecting)
		{
			PhotonNetwork.JoinRandomRoom();
			isConnecting = false;
		}
	}
	public override void OnDisconnected(DisconnectCause cause)
	{
		base.OnDisconnected(cause);
		Debug.LogWarningFormat("On disconect reason {0}", cause);
	}

	public override void OnJoinRandomFailed(short returnCode, string message)
	{
		Debug.Log("OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");
		PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = maxPlayersPerRoom });
	}

	public override void OnLeftRoom()
	{
		base.OnLeftRoom();
	}

	public override void OnJoinedRoom()
	{
		Debug.Log("OnJoinedRoom() called by PUN. Now this client is in a room.");
		//PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(Random.Range(0, 2), 2f, Random.Range(0, 2)), Quaternion.identity, 0);
		promise = GameObject.FindGameObjectWithTag("PromiseFactory").GetComponent<PromiseManager>();
		Debug.Log($"PING: {PhotonNetwork.IsMasterClient} PONG: {PhotonNetwork.PlayerList.Length}");
		if (PhotonNetwork.IsMasterClient)
		{ promise.CreatePromise(() => { this.movePlayersIntoScene(); }, 10f); }
	}

	public void movePlayersIntoScene()
	{
		numOfPlayers = PhotonNetwork.CurrentRoom.PlayerCount; //tells you number of players currently in the room
		switch (numOfPlayers)
		{
			case 2:
				if (PhotonNetwork.IsMasterClient)
				{
					players = PhotonNetwork.PlayerList;
					for (int i = 0; i < players.Length; i++)
					{
						Debug.Log($"PENG: player: {players[i]} i: {i} playerActorNumber: {players[i].ActorNumber}");
						playerIDs[i] = players[i].ActorNumber;
					}
					Constants.C.playerID = playerIDs;
					if (PhotonNetwork.IsMasterClient)
						sendToLevel(2);
						//PhotonNetwork.LoadLevel("2Players");
				}
				break;
			case 3:
				if (PhotonNetwork.IsMasterClient)
				{
					players = PhotonNetwork.PlayerList;
					for (int i = 0; i < players.Length; i++)
					{
						playerIDs[i] = players[i].ActorNumber;
					}
					Constants.C.playerID = playerIDs;
					if (PhotonNetwork.IsMasterClient)
						sendToLevel(3);
						//PhotonNetwork.LoadLevel("3Players");
				}
				break;
			default:
				Debug.Log("Not enough players to start the game, waited 60 sec");
				PhotonNetwork.Disconnect();
				UnityEngine.SceneManagement.SceneManager.LoadScene("SplashSM");
				//PhotonNetwork.LoadLevel("SplashSM");
				break;
		}
	}

	private void sendToLevel(int playerCount) {
		Constants.C.updateConstants();

		switch (playerCount) {
			case 2:
				PhotonNetwork.LoadLevel("2Players");
				break;

			case 3:
				PhotonNetwork.LoadLevel("3Players");
				break;

			case 4:
				PhotonNetwork.LoadLevel("4Players");
				break;

			default:
				Debug.Log("invalid peramiter");
				break;
		}
	}
}