using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SinglePlayerOfflineMode : MonoBehaviourPunCallbacks
{
	private void Start()
	{
		Debug.Log("start");
		PhotonNetwork.OfflineMode = true;
	}

	public override void OnConnectedToMaster()
	{
		base.OnConnectedToMaster();
		Debug.Log("Connected to master");
		PhotonNetwork.CreateRoom("offline");
	}

	public override void OnJoinedRoom()
	{
		base.OnJoinedRoom();
		Debug.Log($"we in a room go load single player\noffline mode = {PhotonNetwork.OfflineMode}");
		photonView.RPC("RPCTest", RpcTarget.AllBuffered);
		PhotonNetwork.LoadLevel("LevelGenerationTest");
	}


	[PunRPC]
	public void RPCTest() {
		Debug.Log("you called an RPC here");
	}
}
