using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class movingAverage : MonoBehaviourPunCallbacks
{
    private Queue<double> lagTimes;
    public int numberOfSamples = 10;
    public double average { get; private set; }
    private double sumOfValues = 0;
    public movingAverage()
    {
        Debug.Log("Moving Average Constructor");
        sumOfValues = 0;
        lagTimes = new Queue<double>();
    }
    public void ComputeMovingAverage(double newLagTime)
    {
        sumOfValues += newLagTime;
        lagTimes.Enqueue(newLagTime); //adding latest sample
        if (lagTimes.Count > numberOfSamples) // test if the Q is full
        {
            sumOfValues -= lagTimes.Dequeue(); //remove oldest value, subtract it from sum
        }
        average = sumOfValues / lagTimes.Count; //use count as there may be <10 times in Q
    }
// Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
