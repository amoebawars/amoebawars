using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class FlagBehavior : MonoBehaviour
{
	public Sprite[] flagColors;
	//1grey		2blue		3purple		4green		5yellow
	public int flagSet = 1;
	//1grey		2blue		3purple		4green		5yellow
	private SpriteRenderer currentSprite;
	public bool isPickable = true;
	private PromiseManager promise;

	// Start is called before the first frame update
	void Start()
	{
		currentSprite = this.GetComponent<SpriteRenderer>();
		currentSprite.sprite = flagColors[0];
		setFlagColor(0);
		promise = GameObject.FindGameObjectWithTag("PromiseFactory").GetComponent<PromiseManager>();
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void setFlagColor(int flagColor) {
		flagSet = flagColor + 1;
		currentSprite.sprite = flagColors[flagColor];
	}

	public void setParent(GameObject par, int color) {
		if (isPickable) {			
			this.transform.parent = par.transform;

			this.transform.position = par.transform.Find("ShootPointNorth").transform.position;

			setFlagColor(color);

			isPickable = false;

			//PhotonView.Get(this).RPC("RPCFlagPickableOverride", RpcTarget.OthersBuffered, false);
		}
	}

	
	public void dropFlag() {
		PhotonView.Get(this).RPC("dropAllFlag", RpcTarget.AllBuffered);
	}

	[PunRPC]
	public void dropAllFlag() {
		promise.CreatePromise(() => { this.refreshFlagPickup(); }, 5f);
		this.transform.parent = null;
	}

	[PunRPC]
	public void RPCFlagPickableOverride(bool state) {
		isPickable = state;
	}

	public void refreshFlagPickup() {
		isPickable = true;
		Debug.Log("flag is pickable");
	}
}
