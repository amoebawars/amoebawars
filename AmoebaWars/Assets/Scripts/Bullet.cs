using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Bullet : MonoBehaviourPunCallbacks
{
	public float speed = 7f;
	public Vector2 direction;
	public GameObject parent;

	private GameObject player;
	private PromiseManager promise;

	AudioSource explosion;
	// Start is called before the first frame update
	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		promise = GameObject.FindGameObjectWithTag("PromiseFactory").GetComponent<PromiseManager>();

		promise.CreatePromise(() => { destroyBullet(); }, 2f);

		explosion = this.GetComponent<AudioSource>();
		explosion.Play();
	}

	// Update is called once per frame
	void Update()
	{
		direction.Normalize();
		Vector2 futurePosition = lagCompensation(this.transform.position, direction);
		transform.position = Vector3.Lerp(futurePosition, this.transform.position, 0.7f);
		//transform.position += new Vector3(direction.x, direction.y, 0) * speed * Time.deltaTime;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		GameObject go = collision.gameObject;
		if (go.tag == "Player")
		{
			Debug.Log("bullet collider with player");
			collision.gameObject.GetComponent<PlayerHealth>().doPlayerDamage();
		}
		return;
	}

	public void destroyBullet()
	{
		if (this != null && photonView.IsMine)
		{
			PhotonNetwork.Destroy(this.gameObject);
		}
	}

	private Vector2 lagCompensation(Vector2 currentPosition, Vector2 directionMoving)
	{
		Vector2 compensatedPosition;
		double currentLag = player.GetComponent<PlayerMovement>().lag;
		compensatedPosition = currentPosition + directionMoving * (float)currentLag;
		return compensatedPosition;
	}
}
